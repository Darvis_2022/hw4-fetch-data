const API = 'https://ajax.test-danit.com/api/swapi/films';
const peopleUrl = "https://ajax.test-danit.com/api/swapi/people/";
const filmsList = document.querySelector('.films_list');
 

function renderFilms(url){
    fetch(url)
    .then(response => response.json())
    .then( data =>{
        data.forEach(({id, name, episodeId,openingCrawl,characters}) => {
            fetch(peopleUrl) 
                .then(response=>response.json())
                .then(people=>{
                    const filterArray = people.filter(actor=>characters.includes(actor.url))
            filmsList.insertAdjacentHTML('beforeend',
            `<li><h3 class="film_name">${id}."${name}"</h3>
                <ul class="characters_list"> <h4>Characters:</h4>
                    
                </ul>
            <p>episode №${episodeId}</p>
            <p style="font-style: italic;">"${openingCrawl}"</p></li>`)
                
               let charactersList = document.querySelector('.characters_list');
                    filterArray.forEach(({name})=>{
                        charactersList.insertAdjacentHTML('beforeend',`
                        <li>${name}</li>`)
                    })
                    
                
            });
        });
       
})}
renderFilms(API);